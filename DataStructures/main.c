#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int val;
    struct node *next;
} node_t;


//STACK
void add(node_t *head, int val)
{
    node_t *current = head;
    while (current->next != NULL)
    {
        current = current->next;
    }

    current->next = (node_t *) malloc(sizeof(node_t));
    current->next->val = val;
    current->next->next = NULL;
}

int pop(node_t *head)
{
    int returnVal;
    if (head->next == NULL)
    {
        returnVal = head->val;
        free(head);
        return returnVal;
    }

    node_t *current = head;
    while (current->next->next != NULL)
    {
        current = current->next;
    }

    returnVal = current->next->val;
    free(current->next);
    current->next = NULL;
    return returnVal;
}

//QUEUE
void enqueue(node_t **head, int val)
{
    node_t *newNode;
    newNode = (node_t *) malloc(sizeof(node_t));

    newNode->val = val;
    newNode->next = *head;
    *head = newNode;
}

int dequeue(node_t **head)
{
    int returnVal;
    node_t *next = NULL;

    if (*head == NULL)
    {
        return -1;
    }

    next = (*head)->next;
    returnVal = (*head)->val;
    free(*head);
    *head = next;

    return returnVal;
}

void printList(node_t *head)
{
    node_t *current = head;

    while (current != NULL)
    {
        printf("%d\n", current->val);
        current = current->next;
    }
}

int removeByIndex(node_t **head, int n)
{
    int i;
    int returnVal;
    node_t *current = *head;
    node_t *tmp = NULL;

    if (n == 0)
    {
        return dequeue(head);
    }

    for (i = 0; i < n - 1; i++)
    {
        if (current->next == NULL)
        {
            return -1;
        }
        current = current->next;
    }

    tmp = current->next;
    returnVal = tmp->val;
    current->next = tmp->next;
    free(tmp);

    return returnVal;
}

int removeByValue(node_t **head, int val)
{
    node_t *current = *head;
    node_t *tmp = NULL;
    node_t *prev = *head;

    if (current->val == val)
    {
        return dequeue(head);
    }

    while (current->next != NULL)
    {
        prev = current;
        current = current->next;

        if (current->val == val)
        {
            prev->next = current->next;
            free(tmp);
            return 1;
        }
    }

    return -1;
}

int main()
{
    node_t *head = NULL;

    head = (node_t *) malloc(sizeof(node_t));
    head->val = 0;

    for (int i = 1; i < 10; ++i)
    {
        add(head, i);
    }

    enqueue(&head, 123);

//    removeByIndex(&head, 0);
    removeByValue(&head, 123);
    printList(head);

    printf("Hello, World\n");
    return 0;
}
