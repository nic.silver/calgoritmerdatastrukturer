#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>


int sortPart(int *p, int l, int h)
{
    int pivot = *(p + h);
    int i = l - 1;

    for (int j = l; j <= h - 1; j++)
    {
        if (*(p + j) < pivot)
        {
            i++;
            int tmp = *(p + i);
            *(p + i) = *(p + j);
            *(p + j) = tmp;
        }
    }
    int tmp = *(p + i + 1);
    *(p + i + 1) = *(p + h);
    *(p + h) = tmp;
    return i + 1;
}

void quickSort(int *p, int l, int h)
{
    if (l < h)
    {
        int parted = sortPart(p, l, h);

        quickSort(p, l, parted - 1);
        quickSort(p, parted + 1, h);
    }
}

int main()
{
    printf("Hello, World!\n");
    int arr[] = {408, 406, 423, 239, 486, 542, 211, 108, 659, 628, 471, 572, 446, 593, 629, 652, 607, 527, 265, 566,
                 582, 508, 589, 443, 565, 586, 649, 140, 293, 536, 270, 639};

    int arrSize = (sizeof(arr) / sizeof *arr);

    printf("\n");
    for (int i = 0; i < arrSize; ++i)
    {
        printf("%i ", arr[i]);
    }

    quickSort(arr, 0, arrSize - 1);
    printf("\n");

    for (int i = 0; i < arrSize; ++i)
    {
        printf("%i ", arr[i]);
    }


    srand(time(NULL)); // NOLINT(cert-msc51-cpp)


    int size = 1000000;
    int *arr1 = malloc(sizeof(int) * size);

//    int arr1[size];
//    int arr2[size];

    clock_t start, end;

    start = clock();
    double timeTaken;
    for (int i = 0; i < size; ++i)
    {
        int b = rand() % 1000; //rnd int from 0-n NOLINT(cert-msc50-cpp)
//        arr1[i] = b;
//        arr2[i] = b;
        *(arr1 + i) = b;
    }
    end = clock();
    timeTaken = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("\nsort took %f seconds to execute \n", timeTaken);

    start = clock();

    quickSort(arr1, 0, size - 1);

    end = clock();
    timeTaken = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("\nsort took %f seconds to execute \n", timeTaken);


    return 0;
}


