#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void reverse(char *p, int length) // This should be very fast!
{
    char tmp;
    for (int i = 0; i < length / 2; ++i)
    {
        tmp = *(p + i);
        *(p + i) = *(p + length - i - 2);
        *(p + length - i - 2) = tmp;
    }
}

void sort(int *p, int length) // Dont know which sort this is, I think its not bubble sort?
{
    length = length - 1;
    int tmp;
    bool valFound = false; // Stupid hack, this makes sure if the last value is bigger than the first value, stuff still works

    for (int i = 0; i < length; ++i)
    {
        tmp = *(p + length - i);
        int pointer = 0;
        for (int j = 0; j < length - i; ++j)
        {
            if (*(p + j) >= tmp)
            {
                tmp = *(p + j);
                pointer = j;
                valFound = true;
            }
        }
        if (valFound)
        {
            int swapValue = *(p + length - i);
            *(p + length - i) = tmp;
            *(p + pointer) = swapValue;
        }
    }
}

int wordCount(const char *p, int length) // A better solution prolly exists
{
    int count = 0;
    for (int i = 0; i < length; ++i)
    {
        if (*(p + i) == ' ' && *(p + i + 1) != ' ')
        {
            count++;
        }
    }
    return count;
}

int main()
{
    // Opg 2
    char rambuk[] = "rambuk";
    printf("%s\n", rambuk);
    reverse(rambuk, sizeof rambuk / sizeof *rambuk);
    printf("%s\n", rambuk);

    // Opg 3
    int numbers[] = {44444544, 3, 2, 3, 401, 1, 1, 2, 44488};
    printf("\nunsorted numbers: ");
    for (int i = 0; i < sizeof numbers / sizeof *numbers; ++i)
    {
        printf("%i ", numbers[i]);
    }
    sort(numbers, sizeof numbers / sizeof *numbers);

    printf("\nsorted numbers:   ");
    for (int i = 0; i < sizeof numbers / sizeof *numbers; ++i)
    {
        printf("%i ", numbers[i]);
    }


    // Opg 4
    char countWord[] = "Hej mit navn er Nicolaj        s ";
    printf("\n\n%s", countWord);
    int numberOfWords = wordCount(countWord, sizeof countWord / sizeof *countWord);
    printf("\nthe word contains %i words\n", numberOfWords);


    // Opg 5
    char words[] = "zzzzz bbbb aaaa tttt";
    char *pch;
    pch = strtok(words, " ");

    char wordTokens[20][50];
    int i = 0;
    printf("\nunsorted: ");
    while (pch != NULL)
    {
        printf("%s ", pch);
        strcpy(wordTokens[i], pch);
        pch = strtok(NULL, " ");
        i++;
    }

    char tmp[50];
    for (int j = 0; j < i; ++j)
    {
        for (int k = j + 1; k < i; ++k)
        {
            if (strcmp(wordTokens[j], wordTokens[k]) > 0)
            {
                strcpy(tmp, wordTokens[j]);
                strcpy(wordTokens[j], wordTokens[k]);
                strcpy(wordTokens[k], tmp);
            }
        }
    }
    printf("\nsorted version: ");

    for (int j = 0; j < i; ++j)
    {
        printf("%s ", wordTokens[j]);
    }

    return 0;
}